
/**
 * Fraction est une classe immuable qui represente un nombre rationnel avec un numerateur et un denominateur
 *
 * @author Amirouche Nassim
 * @version 1.0
 */
public final class Fraction
{
    private final int numerateur;
    private final int denominateur;
    /**
     * Constantes ZERO et UN 
     */
    public final static Fraction ZERO = new Fraction(0, 1);
    public final static Fraction UN = new Fraction(1, 1);
   
    /**
     * Constructor for objects of class Fraction
     */
    public Fraction()
    {
        this(0, 0);
    }
    /**
     * @param numerateur est un int
     */
    public Fraction(int numerateur)
    {
        this(numerateur, 1);
    }
    /**
     * @param numerateur est un int
     * @param denominateur est un int
     */
    public Fraction(int numerateur, int denominateur)
    {
        int pgcd = Fraction.pgcd(numerateur,denominateur);
        this.numerateur = numerateur / pgcd;
        this.denominateur = denominateur / pgcd;
    }

    /** La fonction renvoie le numerateur de la fraction
     * @return int
     */
    public int getNumerateur()
    {
        return numerateur;
    }
    /** La fonctione renvoie le denominateur de la fraction
     * @return int
     */
    public int getDenominateur()
    {
        return denominateur;
    }
    /** La fonction retourne la valeur de la fraction en virgule flottante
     * @return double
     */
    public double toDouble()
    { 
        return (double) numerateur / denominateur;   
    }
    /** Algorithme qui retourne le pgcd de deux entiers a et b (Pour la fonction simplifie)
     * @param a est un int
     * @param b est un int
     */
    private static int pgcd (int a , int b) { 
        int t,r;
        if (b > a) { 
            t = a; 
            a = b; 
            b = t; 
        } 
        r = 1;
        while (r != 0) { 
            r = a % b; 
            a = b; 
            b = r; 
        } 
        return a ; 
    }
    /** Fonction qui simplifie une fraction (Pour la fonction d'addition)
     * @return void
     */
   
    /** La fonction additione deux fractions
     * @param f1 est de type Fraction
     * @return Fraction
     */
    public Fraction addition(Fraction f1)
    {
        
        if (f1.denominateur == this.denominateur)
        {
            Fraction f = new Fraction(f1.numerateur + this.numerateur, f1.denominateur);
            return f;
        }
        else
        {
            Fraction f = new Fraction( (f1.denominateur * this.numerateur) + (f1.numerateur * this.denominateur), f1.denominateur * this.denominateur);
            return f;
        }
        
    }
    /** La fonction teste si deux fonctions sont égales
     * @param f est de type Fraction
     * @return booleen
     */
    public boolean testEgalite(Fraction f)
    {
         if (this.toDouble() == f.toDouble()) return true;
         return false;
    }
    /** La fonction convertit la fraction en chaine de caractere
     * @return String
     */
    public String toString()
    {  
        return this.numerateur + "/" + this.denominateur;
    }
    /** La fonction fait la comparaison de deux fraction
     * @param f est de type Fraction
     * @return String
     */
    public String Comparaison(Fraction f)
    {
         if (this.toDouble() > f.toDouble())  return this.toString() + " > " + f.toString();
         else if (this.toDouble() < f.toDouble())    return this.toString() + " < " + f.toString();
         return this.toString() + " =" + f.toString();
    }
    /** La fonction verifie si this est plus petit que f
     * @param f est de type Fraction
     * @return boolean
     */
    public boolean plusPetitQue(Fraction f)
    {
        if (this.toDouble() <= f.toDouble()) return true;
        return false;
    }
  
}
